﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;


namespace XmlDeserializeSample1
{
    class Program
    {
        static void Main(string[] args)
        {
            string t = @"<XmlTest xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' Prop1='testatt'>
                           
                            <Prop2>Test2</Prop2>
                        </XmlTest>";

            XmlSerializer s = new XmlSerializer(typeof(XmlTest));

            TextReader tr = new StringReader(t);

            var y = s.Deserialize(tr);

        }
    }

    //[XmlRoot(Namespace = "")]
    public class XmlTest
    {
        [XmlAttribute]
        public string Prop1 { get; set; }

        [XmlElement]
        public string Prop2 { get; set; }
    }
}
