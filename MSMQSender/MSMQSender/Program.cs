﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Messaging;

namespace MSMQSender
{
    class Program
    {
        static void Main(string[] args)
        {
            Message recoverableMessage = new Message();
            recoverableMessage.Body = "Sample Recoverable Message";
            recoverableMessage.Recoverable = true;
            MessageQueue msgQ = new MessageQueue(@"ajit6246\private$\localmessage");
            //msgQ.Send(recoverableMessage);

            var message = msgQ.ReceiveById(@"2f7b594c-cecb-493b-ba98-d5a58e10f8d3\1");

            var messages = msgQ.GetAllMessages();

            msgQ.Close();
        }
    }
}
