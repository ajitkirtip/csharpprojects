﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstMEF.Common;

namespace FirstMEF.Operations.ToLower
{
    [Export(typeof(IOperation))]
    [ExportMetadata("Command", "lower")]
    public class ToLower : IOperation
    {
        public string Operate(string data)
        {
            return data.ToLower();
        }
    }
}
