﻿namespace FirstMEF.Common
{
    public interface IOperation
    {
        string Operate(string data);
    }
}