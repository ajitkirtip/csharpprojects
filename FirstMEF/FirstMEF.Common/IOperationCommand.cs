﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstMEF.Common
{
    public interface IOperationCommand
    {
        string Command { get; }
    }
}
