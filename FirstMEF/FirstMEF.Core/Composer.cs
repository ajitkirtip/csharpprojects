﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace FirstMEF.Core
{
    public class Composer
    {
        private CompositionContainer _compositionContainer;

        [Import(typeof(ICore))]
        public ICore Core;

        public Composer()
        {
            AggregateCatalog agreegateCatalog = new AggregateCatalog();
            agreegateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(Composer).Assembly));
            agreegateCatalog.Catalogs.Add(new DirectoryCatalog("Extensions"));

            _compositionContainer = new CompositionContainer(agreegateCatalog);

            try
            {
                _compositionContainer.ComposeParts(this);
            }
            catch (CompositionException exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}