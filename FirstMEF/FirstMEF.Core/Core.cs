﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstMEF.Common;

namespace FirstMEF.Core
{
    [Export(typeof(ICore))]
    public class Core : ICore
    {
        [ImportMany]
        private IEnumerable<Lazy<IOperation, IOperationCommand>> _operations;
        public string PerformOperations(string data, string command)
        {
            foreach (Lazy<IOperation, IOperationCommand> operation in _operations)
            {
                if (operation.Metadata.Command.Equals(command))
                    return operation.Value.Operate(data);
            }

            return "Unrecognized operation";
        }
    }
}
