using System;

namespace FirstMEF.Core
{
    public interface ICore
    {
       String PerformOperations(string data, string command);
    }
}