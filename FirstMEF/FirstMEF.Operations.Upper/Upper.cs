﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstMEF.Common;

namespace FirstMEF.Operations.Upper
{
    [Export(typeof(IOperation))]
    [ExportMetadata("Command", "upper")]
    public class UpperCase : IOperation
    {
        public string Operate(string data)
        {
            return data.ToUpper();
        }
    }
}
